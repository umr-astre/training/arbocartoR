---
title: "arbocartoR used retrospectively"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{arbocartoR used retrospectively}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```


On September 8, 2021, a first case of Dengue outbreak was detected in a french city: Grenoble (38 - Isère). The infected person started developing symptoms on September 4, she lives in street 'rue Pasteur' (parcel: '384850102' ; longitude: 5,69844156503677 ; latitude: 45,1845349969546) and was not coming back from any endemic country. On September 10, area Spraying was implemented in a 500m area around her residence building. From September 13 to 16, door-to-door canvassing was organized in that same area to raise public awareness.

Three weeks later, between October 1 and 3, five youngsters developed symptoms and tested positive for the dengue virus. They lived in the following parcels: 384850102, 381850305, 381850111, and two of them in 381850302. They were all going to the same university: Université Grenoble Alpes (parcel: 381850203). On October 5 and 7, area spraying activities were implemented around the university (latitude: 45,1895937230587, longitude: 5,72210937738419) within a 100m-radius. Door-to-door canvassing was also organized in all the parcels with detected cases. 

No further cases were detected.

Use arbocartoR to try to better understand what happened and if anything could have been done differently.
