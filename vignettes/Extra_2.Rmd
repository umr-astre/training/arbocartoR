---
title: "Get meteorological data"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Get meteorological data}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

Mosquitoes vector population dynamics is highly dependent on meteorological conditions. In arbocartoR, the underlying model takes into account the daily rainfall and temperature.

You can either use data from meteorological stations or data from climatic models.

### Use meteorological station
If you have access to meteorological station records, you can use the tool/application to attribute a meteorological station (the closest one) to each studies spatial polygon. The temperature will be corrected based on the altitude of the meteorological station and the average altitude of the spatial unit.

The environmental dataset must contain both 'STATION' and 'STATION_ALT' columns. In the meteorological dataset, the 'ID' column correspond to the ID of the station.

### Use ERA5-land data
If you don't have access to meteorological station records, you can use ERA5-Land data, a reanalysis dataset providing a consistent view of the evolution of land variables over several decades. 
When using such reanalysis dataset, the environmental dataset must not contain neither 'STATION' nor 'STATION_ALT' column. In the meteorological dataset, the 'ID' column correspond to the ID of the administrative division.

#### Get the ERA5-land data
Copernicus, the Earth Observation component of the European Union's space programme, is in the process of migrating his CDS to a new infrastructure therefore the downloading requests are longer than expected. 
Be patient.
Until then we used the [KrigR package](https://github.com/ErikKusch/KrigR), this one needs some updates and cannot be used properly at the moment. 
In the  meantime, you can directly use the [API of Copernicus](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-land).

<!-- , if you are interested in this approach please contact us to receive the step-by-step tutorial to download the data, and the R scritp to aggregate them into the proper format. -->

##### Dowload the ERA5-land data
On the [Copernicus website](https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-land), access the 'Download' data panel.
![screenshot overview panel](../man/figures/extra_2_screenshot_copernicus_overview.png)
Select the following variables: '2m temperature' (in 'Temperature') and 'total precipitation' (in 'Wind, Pressure and Precipitation').
![screenshot variable panel](../man/figures/extra_2_screenshot_copernicus_variables.png)

Select the desired year, month and days (you can use the 'Select all' checkbox for days).
In the 'Time' section, check all hours using the 'Select all' wheck box.

In the 'Geographical area' section, select the 'Sub-region extraction' option and set the studied extent.
If you don't know your extent, you can find it in the summary displayed in the app when loading your spatial polygons:
![screenshot summary extent](../man/figures/extra_2_screenshot_copernicus_extent.png)

In the format section, select 'Zipped NetCDF-3 (experimental)'.

Click on the 'Submit Form' green button that appear on the bottom of the page and be patient.

##### Format the dataset

*in progress*
