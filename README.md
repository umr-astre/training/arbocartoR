# arbocartoR <img src="man/figures/arbocartoR_logo.png" align="right" style="width: 100px;" alt="Modeling the risk of emergence of Aedes-borne diseases to support vector surveillance and control." />

<!-- badges: start -->
[![CRAN_Status_Badge](http://www.r-pkg.org/badges/version/arbocartoR)](https://cran.r-project.org/package=arbocartoR)
[![pipeline status](https://forgemia.inra.fr/umr-astre/training/arbocartoR/badges/main/pipeline.svg)](https://forgemia.inra.fr/umr-astre/training/arbocartoR/-/commits/main)
[![crashbayes status badge](https://cirad-astre.r-universe.dev/badges/arbocartoR)](https://cirad-astre.r-universe.dev)
<!-- badges: end -->

The arbocartoR user interface is a decision support tool developed at Cirad (France) in the frame of the H2020 MOOD project (https://mood-h2020.eu/). The tool allows to simulate and explore spatial and temporal population dynamics of Aedes mosquitoes (albopictus and aegypti) in various environments and the dynamics of transmission of three arboviruses: dengue, zika, chikungunya. It also provides key figures to support the decision process in implementing surveillance and control. Find out about the origins of arbocartoR at https://www.arbocarto.fr/en

The MOOD project aims to develop innovative tools and services in close collaboration with practitioners working in the early detection, assessment, and monitoring of current and potential infectious disease threats in Europe. The project involves feedback sessions and demos of the tools under development.

MOOD innovations aim to increase the operational abilities of epidemic intelligence systems to face new disease threats, including emerging diseases of known or unknown origins, and antimicrobial resistance pathogens. Through big data and disease modelling innovations, the MOOD project addresses the challenges of cross-sectoral data sharing and valorisation in a One Health framework based on multi-disciplinary collaboration for animal, human, and environmental health. Human and veterinary public-health agencies responsible for designing and implementing strategies to mitigate the identified risks are the end-users.

This pages contain training materials for a ~2 days practical course to introduce you to the use of the graphic user interface of arbocartoR.